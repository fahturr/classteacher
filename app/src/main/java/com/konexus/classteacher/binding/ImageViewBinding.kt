package com.konexus.classteacher.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("setImageWithDrawable")
fun ImageView.setImageWithDrawable(resource: Int) {
    setImageResource(resource)
}

