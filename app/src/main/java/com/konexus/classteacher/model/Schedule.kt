package com.konexus.classteacher.model

class Schedule(
    var mapel1: String = "",
    var time1: String = "00:00",
    var mapel2: String = "",
    var time2: String = "00:00",
    var mapel3: String = "",
    var time3: String = "00:00",
    var mapel4: String = "",
    var time4: String = "00:00",
    var mapel5: String = "",
    var time5: String = "00:00",
    var mapel6: String = "",
    var time6: String = "00:00",
    var mapel7: String = "",
    var time7: String = "00:00",
    var mapel8: String = "",
    var time8: String = "00:00",
    var judul : String = ""
)