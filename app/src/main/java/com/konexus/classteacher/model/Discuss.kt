package com.konexus.classteacher.model

import android.graphics.Bitmap

class Discuss (
    var imageDiscuss: Int = 0,
    var nameDiscuss: String = "",
    var timeDiscuss: String = "",
    var titleDiscuss: String = "",
    var postDiscuss: String = "",
    var imagestatusdiscuss: Bitmap? = null
        )