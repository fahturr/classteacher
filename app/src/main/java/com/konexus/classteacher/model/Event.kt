package com.konexus.classteacher.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class Event(
    var image: Int = 0
) : Parcelable