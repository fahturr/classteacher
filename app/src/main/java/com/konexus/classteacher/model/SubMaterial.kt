package com.konexus.classteacher.model

class SubMaterial (
    val subLessonvideo: String = "",
    val subImagevideo: Int = 0,
    val subLessondownload: String = "",
    val subImagedownload: Int = 0
        )