package com.konexus.classteacher.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class Image(
    var image: Int = 0
) : Parcelable