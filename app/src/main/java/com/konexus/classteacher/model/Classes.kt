package com.konexus.classteacher.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Classes(
    var title: String = "",
    var img: Int = 0
) : Parcelable