package com.konexus.classteacher.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class School(
    var image: Int = 0,
    var name: String = ""
) : Parcelable