package com.konexus.classteacher

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    val loadingEnabled = MutableLiveData<Boolean>()
    val action = MutableLiveData<String>()

}