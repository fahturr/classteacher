package com.konexus.classteacher.ui.teacher.assigment.inprogress

import com.konexus.classteacher.BaseViewModel

class InprogressViewModel : BaseViewModel() {

    companion object {

        const val BACK_ONCLICK = "back_onclick"
    }

    fun backOnclick(){
        action.value = BACK_ONCLICK
    }
}