package com.konexus.classteacher.ui.teacher.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.konexus.classteacher.databinding.FragmentProfileBinding
import com.konexus.classteacher.ui.teacher.profile.*
import com.konexus.classteacher.ui.teacher.profile.menu.*

class ProfileFragment : Fragment() {

    private val profileViewModel: ProfileViewModel by viewModels()
    private var _binding: FragmentProfileBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            lifecycleOwner = this@ProfileFragment
            viewModel = profileViewModel

        }

        profileViewModel.action.observe(viewLifecycleOwner, { action ->
            when(action){
                ProfileViewModel.ACTION_ACCOUNT_ONCLICK -> actionAccountOnclick()
                ProfileViewModel.ACTION_NOTIF_ONCLICK -> actionNotifOnclick()
                ProfileViewModel.ACTION_SETTING_ONCLICK -> actionSettingOnclick()
                ProfileViewModel.ACTION_SECURITY_ONCLICK -> actionSecurityOnclick()
                ProfileViewModel.ACTION_ABOUT_ONCLICK -> actionAboutOnclick()
                ProfileViewModel.ACTION_LOGOUT_ONCLICK -> actionLogoutOnclick()
            }
        })


    }

    private fun actionLogoutOnclick() {

    }

    private fun actionAboutOnclick() {
        startActivity(Intent(requireContext(), AboutActivity::class.java))
    }

    private fun actionSecurityOnclick() {
        startActivity(Intent(requireContext(), SecurityActivity::class.java))
    }

    private fun actionSettingOnclick() {
        startActivity(Intent(requireContext(), SettingActivity::class.java))
    }

    private fun actionNotifOnclick() {
        startActivity(Intent(requireContext(), NotificationActivity::class.java))
    }

    private fun actionAccountOnclick() {
        startActivity(Intent(requireContext(), AccountActivity::class.java))
    }
}