package com.konexus.classteacher.ui.teacher.classes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.konexus.classteacher.databinding.BottomSheetClassesBinding

class BottomSheetClass(
    private val classViewModel: ClassViewModel
) : BottomSheetDialogFragment() {

    companion object {
        const val TAG = "BottomSheetClass"
    }

    private lateinit var binding: BottomSheetClassesBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = BottomSheetClassesBinding.inflate(inflater, container, false)

        binding.apply {
            lifecycleOwner = this@BottomSheetClass
            viewModel = classViewModel
        }

        return binding.root
    }

}