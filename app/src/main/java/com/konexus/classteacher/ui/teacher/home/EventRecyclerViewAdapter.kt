package com.konexus.classteacher.ui.teacher.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemEventAddBinding
import com.konexus.classteacher.databinding.ItemEventBinding
import com.konexus.classteacher.model.Event

class EventRecyclerViewAdapter(
    private val homeViewModel: HomeViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val listEvent = arrayListOf<Event>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            1 -> ViewHolderAdd(
                ItemEventAddBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> ViewHolder(
                ItemEventBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, itemPosition: Int) {
        when (holder.itemViewType) {
            1 -> {
                val itemHolderAdd = holder as ViewHolderAdd

                itemHolderAdd.bindingAdd.apply {
                    viewModel = homeViewModel
                }
            }
            else -> {
                val itemHolder = holder as ViewHolder

                itemHolder.binding.apply {
                    viewModel = homeViewModel
                    position = itemPosition
                    data = listEvent[itemPosition - 1]
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> 1
            else -> 2
        }
    }

    override fun getItemCount(): Int = listEvent.size + 1

    inner class ViewHolder(val binding: ItemEventBinding) :
        RecyclerView.ViewHolder(binding.root)

    inner class ViewHolderAdd(val bindingAdd: ItemEventAddBinding) :
        RecyclerView.ViewHolder(bindingAdd.root)

}