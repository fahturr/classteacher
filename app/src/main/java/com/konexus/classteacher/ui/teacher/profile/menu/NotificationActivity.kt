package com.konexus.classteacher.ui.teacher.profile.menu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityNotificationBinding

class NotificationActivity : AppCompatActivity() {

    private lateinit var notifActivityBinding: ActivityNotificationBinding
    private val notifViewModel : NotificationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notifActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_notification)

        notifActivityBinding.apply {
            lifecycleOwner = this@NotificationActivity
            viewModel = notifViewModel
        }

        notifViewModel.action.observe(this, { action ->
            when(action){
                NotificationViewModel.BACK_ONCLICK_PROFILE -> backOnclickProfile()
            }
        })
    }

    private fun backOnclickProfile() {
        finish()
    }
}