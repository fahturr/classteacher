package com.konexus.classteacher.ui.teacher.profile


import com.konexus.classteacher.BaseViewModel

class ProfileViewModel : BaseViewModel() {

   companion object {
      const val ACTION_ACCOUNT_ONCLICK = "action_account_onclick"
      const val ACTION_NOTIF_ONCLICK = "action_notif_onclick"
      const val ACTION_SETTING_ONCLICK = "action_setting_onclick"
      const val ACTION_SECURITY_ONCLICK = "action_security_onclick"
      const val ACTION_ABOUT_ONCLICK = "action_about_onclick"
      const val ACTION_LOGOUT_ONCLICK = "action_logout_onclick"
   }

   fun accountOnclick(){
       action.value = ACTION_ACCOUNT_ONCLICK
   }
   fun notifOnclick(){
      action.value = ACTION_NOTIF_ONCLICK
   }
   fun settingOnclick(){
      action.value = ACTION_SETTING_ONCLICK
   }
   fun securityOnclick(){
      action.value = ACTION_SECURITY_ONCLICK
   }
   fun aboutOnclick(){
      action.value = ACTION_ABOUT_ONCLICK
   }
   fun logoutOnclick(){
      action.value = ACTION_LOGOUT_ONCLICK
   }
}