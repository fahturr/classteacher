package com.konexus.classteacher.ui.teacher.scheduleboard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityScheduleBoardBinding
import com.konexus.classteacher.ui.teacher.assigment.HomeworkTaskListActivity

class ScheduleBoardActivity : AppCompatActivity() {

    private lateinit var scheduleBoardActivityBinding: ActivityScheduleBoardBinding
    private val scheduleBoardViewModel: ScheduleBoardViewModel by viewModels()
    private lateinit var adapterJadwal: ScheduleRecyclerViewAdapter
    private lateinit var adapterAbsen: AbsenScheduleRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scheduleBoardActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_schedule_board)

        scheduleBoardActivityBinding.apply {
            lifecycleOwner = this@ScheduleBoardActivity
            viewModel = scheduleBoardViewModel
        }

        adapterJadwal = ScheduleRecyclerViewAdapter(scheduleBoardViewModel)
        adapterAbsen = AbsenScheduleRecyclerViewAdapter(scheduleBoardViewModel)

        setupRecyclerView()
        setupRecyclerViewAbsen()

        scheduleBoardViewModel.action.observe(this) { action ->
            when (action) {
                ScheduleBoardViewModel.BACK_ONCLICK_SCHEDULEBOARD -> backOnclickScheduleBoard()
                ScheduleBoardViewModel.GET_IMAGE_SCHEDULEBOARD -> getImageScheduleBoard()
                ScheduleBoardViewModel.ACTION_LIST_SCHEDULE_UPDATE -> actionListScheduleUpdate()
                ScheduleBoardViewModel.ACTION_LIST_ABSENT_UPDATE -> actionListAbsentUpdate()
                ScheduleBoardViewModel.MAPEL1_ONCLICK -> mapel1Onclick()
                ScheduleBoardViewModel.ABSEN_ONCLICK -> absenOnclick()
            }
        }
        scheduleBoardViewModel.fetchScheduleList()
        scheduleBoardViewModel.fetchAbsentList()
    }

    private fun mapel1Onclick() {
        val editText = EditText(this)
        MaterialAlertDialogBuilder(this)
            .setTitle("Input Your Class")
            .setView(editText)
            .setPositiveButton("OK") { dialog, which ->
                scheduleBoardViewModel.listSchedule[scheduleBoardViewModel.schedulePosition.value
                    ?: 0].mapel1 = editText.text.toString()
                adapterJadwal.notifyDataSetChanged()


            }
            .setNegativeButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            .show()

    }

    private fun absenOnclick() {
//        Toast.makeText(this, "halo", Toast.LENGTH_SHORT).show()
//        val builder = AlertDialog.Builder(this)
//        builder.setTitle("Absen Murid")
//        val absen = arrayOf("Hadir", "Sakit", "Absen", "Izin")
//        var checkedItem = 0
//        builder.setSingleChoiceItems(absen, checkedItem) { dialog, which ->
//            when (which) {
//
//                0 -> checkedItem = 0
//                1 -> checkedItem = 1
//                2 -> checkedItem = 2
//                3 -> checkedItem = 3
//            }
//        }
//
//        builder.setPositiveButton("OK") { dialog, which ->
//            when (checkedItem) {
//                0 -> {
//                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
//                        ?: 0].image = R.drawable.hadirabsen
//                }
//                1 -> {
//                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
//                        ?: 0].image = R.drawable.sakitabsen
//                }
//                2 -> {
//                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
//                        ?: 0].image = R.drawable.bolosabsen
//                }
//                3 -> {
//                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
//                        ?: 0].image = R.drawable.izinabsen
//                }
//                else -> {
//                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
//                        ?: 0].image = R.drawable.absentschedule
//                }
//            }
//            adapterAbsen.notifyDataSetChanged()
//        }
//        builder.setNegativeButton("Cancel", null)
//        val dialog = builder.create()
//        dialog.show()

        val singleItems = arrayOf("Hadir", "Bolos", "Izin", "Sakit")
        var checkedItem = 0

        MaterialAlertDialogBuilder(this)
            .setTitle("Absensi Murid")
            .setNeutralButton("Cancel") { dialog, which ->
                // Respond to neutral button press
            }
            .setPositiveButton("OK") { dialog, which ->
                Toast.makeText(this, singleItems[checkedItem], Toast.LENGTH_SHORT).show()
                when(checkedItem){
                    0 -> {
                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
                        ?: 0].image = R.drawable.hadirabsen
                        adapterAbsen.notifyDataSetChanged()
                }
                1 -> {
                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
                        ?: 0].image = R.drawable.bolosabsen
                    adapterAbsen.notifyDataSetChanged()
                }
                2 -> {
                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
                        ?: 0].image = R.drawable.izinabsen
                    adapterAbsen.notifyDataSetChanged()
                }
                3 -> {
                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
                        ?: 0].image = R.drawable.sakitabsen
                    adapterAbsen.notifyDataSetChanged()
                }
                else -> {
                    scheduleBoardViewModel.listAbsent[scheduleBoardViewModel.absenPosition.value
                        ?: 0].image = R.drawable.absentschedule
                    adapterAbsen.notifyDataSetChanged()
                }

                }

            }
            // Single-choice items (initialized with checked item)
            .setSingleChoiceItems(singleItems, checkedItem) { dialog, which ->
                checkedItem = which
            }
            .show()

    }

    private fun setupRecyclerView() {
        scheduleBoardActivityBinding.rvSchedule.apply {
            adapter = adapterJadwal
            layoutManager = LinearLayoutManager(
                this@ScheduleBoardActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        }
    }

    private fun setupRecyclerViewAbsen() {
        scheduleBoardActivityBinding.absensecheduleteacher.apply {
            adapter = adapterAbsen
            layoutManager = LinearLayoutManager(
                this@ScheduleBoardActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }

    private fun actionListScheduleUpdate() {
        adapterJadwal.apply {
            listSchedule.addAll(scheduleBoardViewModel.listSchedule)
            notifyDataSetChanged()
        }
    }

    private fun actionListAbsentUpdate() {
        adapterAbsen.apply {
            listAbsenSchedule.addAll(scheduleBoardViewModel.listAbsent)
            notifyDataSetChanged()
        }
    }

    private fun getImageScheduleBoard() {

    }

    private fun backOnclickScheduleBoard() {
        startActivity(Intent(this, HomeworkTaskListActivity::class.java))
        finish()
    }
}