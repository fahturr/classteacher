package com.konexus.classteacher.ui.teacher.profile.menu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivitySettingBinding

class SettingActivity : AppCompatActivity() {

    private lateinit var settingActivityBinding: ActivitySettingBinding
    private val settingViewModel : SettingViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        settingActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_setting)

        settingActivityBinding.apply {
            lifecycleOwner = this@SettingActivity
            viewModel = settingViewModel
        }
        settingViewModel.action.observe(this, { action ->
            when(action){
                AccountViewModel.BACK_ONCLICK_PROFILE -> backOnclickProfile()
            }
        })
    }

    private fun backOnclickProfile() {
        finish()
    }
}