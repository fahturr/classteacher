package com.konexus.classteacher.ui.teacher.discuss

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityDiscussBinding

class DiscussActivity : AppCompatActivity() {

    private lateinit var discussBinding: ActivityDiscussBinding
    private val discussViewModel: DiscussViewModel by viewModels()
    private lateinit var adapterDiscuss: DiscussRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        discussBinding =
        DataBindingUtil.setContentView(this,R.layout.activity_discuss)

        discussBinding.apply {
            lifecycleOwner = this@DiscussActivity
            viewModel = discussViewModel
        }

        adapterDiscuss = DiscussRecyclerViewAdapter(discussViewModel)

        setupRecyclerView()

        discussViewModel.action.observe(this) { action ->
            when (action) {
                DiscussViewModel.BACK_ONCLICK -> backOnclick()
                DiscussViewModel.CAMERA_ONCLICK -> cameraOnclick()
                DiscussViewModel.GALLERY_ONCLICK -> galleryOnclick()
                DiscussViewModel.LOCATION_ONCLICK -> locationOnclick()
                DiscussViewModel.DOCUMENT_ONCLICK -> documentOnclick()
                DiscussViewModel.TAG_ONCLICK -> tagOnclick()
                DiscussViewModel.ACTION_DISCUSS_LIST_UPDATE -> actionDiscussListUpdate()
            }
        }
        discussViewModel.fetchDiscussList()
    }

    private fun actionDiscussListUpdate() {
        adapterDiscuss.apply {
            discussList.addAll(discussViewModel.discusslist)
            notifyDataSetChanged()
        }
    }

    private fun tagOnclick() {

    }

    private fun documentOnclick() {

    }

    private fun locationOnclick() {

    }

    private fun galleryOnclick() {

    }

    private fun cameraOnclick() {

    }

    private fun backOnclick() {
        finish()
    }

    private fun setupRecyclerView() {
        discussBinding.recyclerdiscuss.apply {
            adapter = adapterDiscuss
            layoutManager = LinearLayoutManager(
                this@DiscussActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }
}