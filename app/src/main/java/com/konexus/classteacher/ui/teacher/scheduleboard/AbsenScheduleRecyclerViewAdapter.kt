package com.konexus.classteacher.ui.teacher.scheduleboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemAbsenteacherBinding
import com.konexus.classteacher.model.Absent

class AbsenScheduleRecyclerViewAdapter (
    private val absenscheduleViewModel : ScheduleBoardViewModel
        ) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

            val listAbsenSchedule = arrayListOf<Absent>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemAbsenteacherBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, itemposition: Int) {
        val itemHolder = holder as AbsenScheduleRecyclerViewAdapter.ViewHolder

        itemHolder.binding.apply {
            viewModel = absenscheduleViewModel
            data = listAbsenSchedule[itemposition]
            position = itemposition
        }
    }

    override fun getItemCount(): Int  = listAbsenSchedule.size

    inner class ViewHolder(val binding: ItemAbsenteacherBinding) :
        RecyclerView.ViewHolder(binding.root)
}