package com.konexus.classteacher.ui.teacher.assigment.complete.complete4

import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.R
import com.konexus.classteacher.model.TaskFinal
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FinalCompleteViewModel : BaseViewModel() {

    companion object{

        const val BACK_ONCLICK = "back_onclick"
        const val ACTION_UPDATE_LIST_COMPLETE4 = "action_update_list_complete4"
    }

    val complete4list = arrayListOf<TaskFinal>()

    fun backOnclick(){
        action.value = BACK_ONCLICK
    }

    fun fetchComplete4List(){
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2500L)

            val tempListComplete4 = getComplete4()

            complete4list.clear()
            complete4list.addAll(tempListComplete4)

            action.postValue(ACTION_UPDATE_LIST_COMPLETE4)
            loadingEnabled.postValue(false)
        }
    }

    private fun getComplete4(): ArrayList<TaskFinal> {
        val getcomplete4 = arrayListOf<TaskFinal>()

        getcomplete4.add(
            TaskFinal(
                R.drawable.john,
                "John D",
                "Statistika",
                "XII-4"
            )
        )
        getcomplete4.add(
            TaskFinal(
                R.drawable.nina,
                "Nina",
                "Statistika",
                "XII-4"
            )
        )
        getcomplete4.add(
            TaskFinal(
                R.drawable.john,
                "John D",
                "Statistika",
                "XII-4"
            )
        )
        getcomplete4.add(
            TaskFinal(
                R.drawable.nina,
                "Nina",
                "Statistika",
                "XII-4"
            )
        )

        return getcomplete4
    }
}