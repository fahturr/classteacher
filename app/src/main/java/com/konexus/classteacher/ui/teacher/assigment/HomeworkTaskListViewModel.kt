package com.konexus.classteacher.ui.teacher.assigment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.R
import com.konexus.classteacher.model.TaskList
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class HomeworkTaskListViewModel : BaseViewModel() {

    companion object{

        const val BACK_ONCLICK_HOMEWORKTASKLIST = "back_onclick_homeworktasklist"
        const val ONGOING_TASK_ONCLICK = "ongoing_task_onclick"
        const val INPROGRESS_TASK_ONCLICK = "inprogress_task_onclick"
        const val COMPLETE_TASK_ONCLICK = "complete_task_onclick"
        const val UPLOAD_TASK_ONCLICK = "upload_task_onclick"
        const val ACTION_TASK_LIST_UPDATE = "action_task_list_update"
    }

    val taskList = arrayListOf<TaskList>()

    val taskPosition = MutableLiveData<Int>()

    fun backOnclickHomeworkTaskList(){
        action.value = BACK_ONCLICK_HOMEWORKTASKLIST
    }
    fun ongoingTaskOnclick(){
        action.value = ONGOING_TASK_ONCLICK
    }
    fun inprogressTaskOnclick(){
        action.value = INPROGRESS_TASK_ONCLICK
    }
    fun completeTaskOnclick(){
        action.value = COMPLETE_TASK_ONCLICK
    }
    fun uploadTaskOnclick(){
        action.value = UPLOAD_TASK_ONCLICK
    }

    fun fetchTaskList(){
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2500L)

            val tempTaskList = getTask()

            taskList.clear()
            taskList.addAll(tempTaskList)

            action.postValue(ACTION_TASK_LIST_UPDATE)
            loadingEnabled.postValue(false)
        }
    }

    private fun getTask(): ArrayList<TaskList> {
        val gettask = arrayListOf<TaskList>()

        gettask.add(
            TaskList(
                R.drawable.john,
                "John D",
                "Submit Task Now"
            )
        )

        gettask.add(
            TaskList(
                R.drawable.nina,
                "Nina",
                "Submit Task 2s"
            )
        )

        return gettask

    }
}