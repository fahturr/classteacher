package com.konexus.classteacher.ui.teacher.assigment.complete.complete3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityScoreCompleteBinding
import com.konexus.classteacher.ui.teacher.assigment.complete.complete2.CorectCompleteActivity
import com.konexus.classteacher.ui.teacher.assigment.complete.complete4.FinalCompleteActivity

class ScoreCompleteActivity : AppCompatActivity() {

    private lateinit var scoreCompleteBinding: ActivityScoreCompleteBinding
    private val scoreCompleteViewModel: ScoreCompleteViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scoreCompleteBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_score_complete)

        scoreCompleteBinding.apply {
            lifecycleOwner = this@ScoreCompleteActivity
            viewModel = scoreCompleteViewModel
        }

        scoreCompleteViewModel.action.observe(this, { action ->
            when(action){
                ScoreCompleteViewModel.BACK_ONCLICK -> backOnclick()
                ScoreCompleteViewModel.ACTION_FINALCOMPLETE_ONCLICK -> finalCompleteOnclick()
            }
        })
    }

    private fun finalCompleteOnclick() {
        startActivity(Intent(this, FinalCompleteActivity::class.java))
    }

    private fun backOnclick() {
        finish()
    }
}