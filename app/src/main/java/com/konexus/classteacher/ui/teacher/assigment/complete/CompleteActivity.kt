package com.konexus.classteacher.ui.teacher.assigment.complete

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityCompleteBinding
import com.konexus.classteacher.ui.teacher.assigment.complete.complete2.CorectCompleteActivity

class CompleteActivity : AppCompatActivity() {

    private lateinit var completeActivityBinding: ActivityCompleteBinding
    private val completeViewModel: CompleteViewModel by viewModels()
    private lateinit var adapterComplete: CompleteRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        completeActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_complete)

        completeActivityBinding.apply {
            lifecycleOwner = this@CompleteActivity
            viewModel = completeViewModel
        }

        adapterComplete = CompleteRecyclerViewAdapter(completeViewModel)

        setupRecyclerView()

        completeViewModel.action.observe(this) { action ->
            when (action) {
                CompleteViewModel.BACK_ONCLICK -> backOnclick()
                CompleteViewModel.ACTION_UPDATE_LIST_COMPLETE1 -> actionUpdateListComplete1()
                CompleteViewModel.ACTION_COMPLETE2_ONCLICK -> actionComplete2Onclick()

            }
        }
        completeViewModel.fetchComplete1List()
    }

    private fun setupRecyclerView() {
        completeActivityBinding.recyclercomplete.apply {
            adapter = adapterComplete
            layoutManager = LinearLayoutManager(
                this@CompleteActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }

    private fun actionUpdateListComplete1() {
        adapterComplete.apply {
            completelist.addAll(completeViewModel.complete1list)
            notifyDataSetChanged()
        }
        
    }

    private fun backOnclick() {
        finish()
    }

    private fun actionComplete2Onclick() {
        startActivity(Intent(this, CorectCompleteActivity::class.java))
    }


}