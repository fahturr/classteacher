package com.konexus.classteacher.ui.teacher.material

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemSubmaterialBinding
import com.konexus.classteacher.model.SubMaterial

class SubMaterialRecyclerViewAdapter (
    private val materialViewModel: MaterialViewModel,
    val submaterialList : List<SubMaterial>
        ): RecyclerView.Adapter<RecyclerView.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemSubmaterialBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, submaterialposition: Int) {
        val submaterialHolderAdd = holder as SubMaterialRecyclerViewAdapter.ViewHolder

        submaterialHolderAdd.binding.apply {
            viewModel = materialViewModel
            data = submaterialList[submaterialposition]
            position = submaterialposition
        }
    }

    override fun getItemCount(): Int = submaterialList.size

    inner class ViewHolder (val binding: ItemSubmaterialBinding):
            RecyclerView.ViewHolder(binding.root)
}