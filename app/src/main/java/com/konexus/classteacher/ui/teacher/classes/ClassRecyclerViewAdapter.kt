package com.konexus.classteacher.ui.teacher.classes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemClassesAddBinding
import com.konexus.classteacher.databinding.ItemClassesBinding
import com.konexus.classteacher.model.Classes

class ClassRecyclerViewAdapter(
    private val classViewModel: ClassViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val listClasses = arrayListOf<Classes>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            1 -> ViewHolderAdd(
                ItemClassesAddBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> ViewHolder(
                ItemClassesBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, itemPosition: Int) {
        when (holder.itemViewType) {
            1 -> {
                val itemHolderAdd = holder as ViewHolderAdd

                itemHolderAdd.bindingAdd.apply {
                    viewModel = classViewModel
                }
            }
            else -> {
                val itemHolder = holder as ViewHolder

                itemHolder.binding.apply {
                    viewModel = classViewModel
                    position = itemPosition
                    data = listClasses[itemPosition - 1]
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> 1
            else -> 2
        }
    }

    override fun getItemCount(): Int = listClasses.size + 1

    inner class ViewHolder(val binding: ItemClassesBinding) :
        RecyclerView.ViewHolder(binding.root)

    inner class ViewHolderAdd(val bindingAdd: ItemClassesAddBinding) :
        RecyclerView.ViewHolder(bindingAdd.root)

}