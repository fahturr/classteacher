package com.konexus.classteacher.ui.teacher.assigment.complete.complete2

import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.R
import com.konexus.classteacher.model.TaskCorrection
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CorectCompleteViewModel : BaseViewModel(){

    companion object{

        const val BACK_ONCLICK = "back_onclick"
        const val ACTION_SCORECOMPLETE_ONCLICK = "action_scorecomplete_onclick"
        const val ACTION_UPDATE_LIST_COMPLETE2 = "action_update_list_complete2"

    }

    val  complete2list = arrayListOf<TaskCorrection>()

    fun backOnclick(){
        action.value = BACK_ONCLICK
    }

    fun scoreComplete(){
        action.value = ACTION_SCORECOMPLETE_ONCLICK
    }

    fun fetchComplete2List(){
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2500L)

            val tempListComplete2 = getComplete2()

            complete2list.clear()
            complete2list.addAll(tempListComplete2)

            action.postValue(ACTION_UPDATE_LIST_COMPLETE2)
            loadingEnabled.postValue(false)
        }
    }

    private fun getComplete2(): ArrayList<TaskCorrection> {
        val getcomplete2 = arrayListOf<TaskCorrection>()

        getcomplete2.add(
            TaskCorrection(
                R.drawable.john,
                "John D",
                "Statistika",
            "XII-4"
            )
        )
        getcomplete2.add(
            TaskCorrection(
                R.drawable.nina,
                "Nina",
                "Statistika",
                "XII-4"
            )
        )

        return getcomplete2

    }
}