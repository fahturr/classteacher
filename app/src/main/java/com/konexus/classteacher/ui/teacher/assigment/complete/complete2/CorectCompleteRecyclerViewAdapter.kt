package com.konexus.classteacher.ui.teacher.assigment.complete.complete2

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemCorrectionBinding
import com.konexus.classteacher.model.TaskCorrection

class CorectCompleteRecyclerViewAdapter(
    private val corectCompleteViewModel: CorectCompleteViewModel
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val corectcompletelist = arrayListOf<TaskCorrection>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemCorrectionBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, corectposition: Int) {
        val corectcompleteHolderAdd = holder as ViewHolder

        corectcompleteHolderAdd.binding.apply {
            viewModel = corectCompleteViewModel
            data = corectcompletelist[corectposition]
            position = corectposition
        }
    }

    override fun getItemCount(): Int = corectcompletelist.size

    inner class ViewHolder(val binding: ItemCorrectionBinding):
            RecyclerView.ViewHolder(binding.root)
}