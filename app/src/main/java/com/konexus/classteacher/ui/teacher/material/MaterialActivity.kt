package com.konexus.classteacher.ui.teacher.material

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityMaterialBinding
import kotlin.math.log

class MaterialActivity : AppCompatActivity() {

    private lateinit var materialBinding: ActivityMaterialBinding
    private val materialViewModel: MaterialViewModel by viewModels()
    private lateinit var adapterMaterial: MaterialRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        materialBinding =
        DataBindingUtil.setContentView(this,R.layout.activity_material)

        materialBinding.apply {
            lifecycleOwner =this@MaterialActivity
            viewModel = materialViewModel
        }

        adapterMaterial = MaterialRecyclerViewAdapter(materialViewModel)

        setupRecyclerView()

        materialViewModel.action.observe(this){action ->
            when(action){
                MaterialViewModel.BACK_ONCLICK -> backOnclick()
                MaterialViewModel.ACTION_UPDATE_LIST_MATERIAL -> actionUpdatelistMaterial()
                MaterialViewModel.ACTION_SUBMATERIAL_ONCLICK -> actionSubmaterialOnclick()
            }
        }
        materialViewModel.fetchMaterialList()
    }

    private fun actionSubmaterialOnclick() {

        materialViewModel.materiallist[materialViewModel.materialposition.value?: 0] .let {
            it.expand =! it.expand

                if (it.expand) {
                    materialViewModel.materiallist[materialViewModel.materialposition.value ?: 0].imageexpand =
                    R.drawable.closeimagesubmaterial
                } else {
                    materialViewModel.materiallist[materialViewModel.materialposition.value ?: 0].imageexpand =
                    R.drawable.imageplusmaterial
                }
            }
        adapterMaterial.notifyDataSetChanged()
        }



    private fun actionUpdatelistMaterial() {
        adapterMaterial.apply {
            materiallist.addAll(materialViewModel.materiallist)
            notifyDataSetChanged()
        }
    }

    private fun backOnclick() {
        finish()
    }

    private fun setupRecyclerView() {
        materialBinding.recyclermaterial.apply {
            adapter = adapterMaterial
            layoutManager = LinearLayoutManager(
                this@MaterialActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }
}