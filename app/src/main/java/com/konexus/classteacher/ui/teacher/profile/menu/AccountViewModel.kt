package com.konexus.classteacher.ui.teacher.profile.menu

import com.konexus.classteacher.BaseViewModel

class AccountViewModel : BaseViewModel() {

    companion object {
        const val BACK_ONCLICK_PROFILE = "back_onclick_profile"
        const val SAVE_CHANGES_ONCLICK = "save_changes_onclick"
        const val CANCEL_CHANGES_ONCLICK = "cancel_changesonclick"
        const val FOTO_PROFILE_ONCLICK = "foto_profile_onclick"
        const val CAMERA_FOTO_ONCLICK = "camera_foto_onclick"
        const val GALLERY_FOTO_ONCLICK = "gallery_foto_onclick"
    }

    fun backOnclickProfile(){
        action.value = BACK_ONCLICK_PROFILE
    }

    fun saveChangesOnclick(){
        action.value = SAVE_CHANGES_ONCLICK
    }

    fun cancelChangesOnclick(){
        action.value = CANCEL_CHANGES_ONCLICK
    }

    fun cameraFotoOnclick(){
        action.value = CAMERA_FOTO_ONCLICK

    }

    fun galleryFotoOnclick(){
        action.value = GALLERY_FOTO_ONCLICK
    }

    fun fotoProfileOnclick(){
        action.value = FOTO_PROFILE_ONCLICK

    }
}