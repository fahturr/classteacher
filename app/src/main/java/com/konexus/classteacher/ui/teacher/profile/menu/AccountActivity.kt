package com.konexus.classteacher.ui.teacher.profile.menu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityAccountBinding

class AccountActivity : AppCompatActivity() {

    private lateinit var accountActivityBinding: ActivityAccountBinding
    private val accountViewModel : AccountViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        accountActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_account)

        accountActivityBinding.apply {
            lifecycleOwner = this@AccountActivity
            viewModel = accountViewModel
        }

        accountViewModel.action.observe(this, { action ->
        when(action){
            AccountViewModel.BACK_ONCLICK_PROFILE -> backOnclickProfile()
            AccountViewModel.FOTO_PROFILE_ONCLICK -> fotoProfileOnclick()
        }
        })
    }

    private fun fotoProfileOnclick() {
        val items = arrayOf("Camera", "Gallery")

        MaterialAlertDialogBuilder(this)
            .setTitle("")
            .setItems(items) { dialog, which ->
                // Respond to item chosen
                when (items[which]){
                    "Camera" -> cameraOnclick()
                    "Gallery" -> galleryOnclick()
                }
            }
            .show()
    }

    private fun galleryOnclick() {
        Toast.makeText(this, "Akses gallery", Toast.LENGTH_SHORT).show()
    }

    private fun cameraOnclick() {
        Toast.makeText(this, "Akses camera", Toast.LENGTH_SHORT).show()
    }

    private fun backOnclickProfile() {
        MaterialAlertDialogBuilder(this)
            .setTitle("Account")
            .setMessage("Keep the change ?")
            .setNegativeButton("Save") { dialog, which ->
                // Respond to negative button press
                dialog.dismiss()
            }
            .setPositiveButton("Discard") { dialog, which ->
                // Respond to positive button press
                finish()
            }
            .show()
    }
}