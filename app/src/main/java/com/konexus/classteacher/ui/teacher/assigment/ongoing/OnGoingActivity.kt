package com.konexus.classteacher.ui.teacher.assigment.ongoing

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityOnGoingBinding

class OnGoingActivity : AppCompatActivity() {

    private lateinit var onGoingBinding: ActivityOnGoingBinding
    private val ongoingViewModel: OngoingViewModel by viewModels()
    private lateinit var adapterOngoing: OngoingRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onGoingBinding =
        DataBindingUtil.setContentView(this,R.layout.activity_on_going)

        onGoingBinding.apply {
            lifecycleOwner = this@OnGoingActivity
            viewModel = ongoingViewModel
        }

        adapterOngoing = OngoingRecyclerViewAdapter(ongoingViewModel)

        setupRecycleViewOngoing()

        ongoingViewModel.action.observe(this){action ->
            when(action){
                OngoingViewModel.ACTION_BACK_ONCLICK -> backOnclick()
                OngoingViewModel.ONGOING_TASK_LIST_UPDATE -> ongoingTaaskListUpdate()
            }
        }
        ongoingViewModel.fetchOngoingList()
    }

    private fun ongoingTaaskListUpdate() {
        adapterOngoing.apply {
            ongoinglist.addAll(ongoingViewModel.ongoingList)
            notifyDataSetChanged()
        }
    }

    private fun backOnclick() {
        finish()
    }

    private fun setupRecycleViewOngoing() {
        onGoingBinding.recyclerongoing.apply {
            adapter = adapterOngoing
            layoutManager = LinearLayoutManager(
                this@OnGoingActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }
}