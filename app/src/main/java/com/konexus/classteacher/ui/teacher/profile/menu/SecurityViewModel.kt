package com.konexus.classteacher.ui.teacher.profile.menu

import com.konexus.classteacher.BaseViewModel

class SecurityViewModel : BaseViewModel() {

    companion object {

        const val BACK_ONCLICK_PROFILE = "back_onclick_profile"
    }

    fun backOnclickProfile(){
        action.value = BACK_ONCLICK_PROFILE
    }
}