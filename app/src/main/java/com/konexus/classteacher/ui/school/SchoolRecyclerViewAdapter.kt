package com.konexus.classteacher.ui.school

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemSchoolAddBinding
import com.konexus.classteacher.databinding.ItemSchoolBinding
import com.konexus.classteacher.model.School

class SchoolRecyclerViewAdapter(
    private val schoolViewModel: SchoolViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val listSchool = arrayListOf<School>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            1 -> ViewHolderAdd(
                ItemSchoolAddBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> ViewHolderItem(
                ItemSchoolBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> 1
            else -> 2
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, itemPosition: Int) {
        when (holder.itemViewType) {
            1 -> {
                val viewHolderAdd = holder as ViewHolderAdd

                viewHolderAdd.bindingAdd.apply {
                    viewModel = schoolViewModel
                }
            }
            else -> {
                val viewHolderItem = holder as ViewHolderItem

                viewHolderItem.bindingItem.apply {
                    data = listSchool[itemPosition - 1]
                    viewModel = schoolViewModel
                    position = itemPosition
                }
            }
        }
    }

    override fun getItemCount(): Int = listSchool.size + 1

    inner class ViewHolderItem(val bindingItem: ItemSchoolBinding) :
        RecyclerView.ViewHolder(bindingItem.root)

    inner class ViewHolderAdd(val bindingAdd: ItemSchoolAddBinding) :
        RecyclerView.ViewHolder(bindingAdd.root)

}