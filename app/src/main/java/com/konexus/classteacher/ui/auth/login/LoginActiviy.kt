package com.konexus.classteacher.ui.auth.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.konexus.classteacher.ui.auth.otp.OtpActivity
import com.konexus.classteacher.ui.auth.register.RegisterListActivity
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityLoginBinding
import com.konexus.classteacher.ui.school.SchoolActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActiviy : AppCompatActivity() {

    private lateinit var loginActivityBinding: ActivityLoginBinding
    private val loginViewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        loginActivityBinding.apply {
            lifecycleOwner = this@LoginActiviy
            viewModel = loginViewModel
        }

        loginViewModel.action.observe(this, { action ->
            when (action) {
                LoginViewModel.ACTION_LOGIN_SIGNUP_CLICK -> signUpOnClick()
                LoginViewModel.ACTION_LOGIN_EMAIL_SUCCESS -> loginEmailSuccess()
                LoginViewModel.ACTION_LOGIN_PHONE_SUCCESS -> loginPhoneSuccess()
            }
        })
    }

    private fun loginPhoneSuccess() {
        startActivity(Intent(this, OtpActivity::class.java))
    }

    private fun loginEmailSuccess() {
        Toast.makeText(
            this,
            "Hello ${loginViewModel.emailEditText.value}! Login Success!",
            Toast.LENGTH_SHORT
        ).show()
        startActivity(Intent(this, SchoolActivity::class.java))
    }

    private fun signUpOnClick() {
        startActivity(Intent(this, RegisterListActivity::class.java))
    }

}