package com.konexus.classteacher.ui.teacher.discuss

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemStatusdiscussBinding
import com.konexus.classteacher.model.Discuss

class DiscussRecyclerViewAdapter (
    private val discussViewModel: DiscussViewModel
        ): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

            val discussList = arrayListOf<Discuss>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemStatusdiscussBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, discussposition: Int) {
        val discussHolderAdd = holder as DiscussRecyclerViewAdapter.ViewHolder

        discussHolderAdd.binding.apply {
            viewModel = discussViewModel
            data = discussList[discussposition]
            position = discussposition
        }
    }

    override fun getItemCount(): Int = discussList.size

    inner class ViewHolder(val binding: ItemStatusdiscussBinding):
            RecyclerView.ViewHolder(binding.root)
}