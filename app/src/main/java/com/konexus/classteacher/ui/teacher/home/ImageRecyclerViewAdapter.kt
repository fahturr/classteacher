package com.konexus.classteacher.ui.teacher.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemImageBinding
import com.konexus.classteacher.model.Image

class ImageRecyclerViewAdapter(
    private val homeViewModel: HomeViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val listImage: ArrayList<Image> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, itemPosition: Int) {
        val viewHolderItem = holder as ViewHolder

        viewHolderItem.binding.apply {
            position = itemPosition
            data = listImage[itemPosition]
            viewModel = homeViewModel
        }
    }

    override fun getItemCount(): Int = listImage.size

    inner class ViewHolder(val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root)

}