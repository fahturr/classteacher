package com.konexus.classteacher.ui.teacher.material.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityViewMaterialBinding

class ViewMaterialActivity : AppCompatActivity() {

    private lateinit var viewMaterialBinding: ActivityViewMaterialBinding
    private val viewViewModel: ViewViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewMaterialBinding =
        DataBindingUtil.setContentView(this, R.layout.activity_view_material)

        viewViewModel.action.observe(this){action ->
        when (action){
            ViewViewModel.BACK_ONCLICK -> backOnclick()

           }
        }
    }

    private fun backOnclick() {
        finish()
    }
}