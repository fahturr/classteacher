package com.konexus.classteacher.ui.teacher.assigment.complete.complete4

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemFinalBinding
import com.konexus.classteacher.model.TaskFinal

class FinalCompleteRecyclerViewAdapter (
    private val finalCompleteViewModel: FinalCompleteViewModel
        ): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val finalcompletelist = arrayListOf<TaskFinal>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemFinalBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, finalposition: Int) {
        val finalcompleteHolderAdd = holder as ViewHolder

        finalcompleteHolderAdd.binding.apply {
            viewModel = finalCompleteViewModel
            data = finalcompletelist[finalposition]
            position = finalposition
        }
    }

    override fun getItemCount(): Int = finalcompletelist.size

    inner class ViewHolder(val binding: ItemFinalBinding):
            RecyclerView.ViewHolder(binding.root)
}