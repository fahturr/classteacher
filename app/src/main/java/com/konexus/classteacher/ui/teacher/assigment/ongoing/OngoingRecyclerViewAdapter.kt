package com.konexus.classteacher.ui.teacher.assigment.ongoing

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemOngoingBinding
import com.konexus.classteacher.model.TaskOngoing

class OngoingRecyclerViewAdapter(
    private val ongoingViewModel: OngoingViewModel
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val ongoinglist = arrayListOf<TaskOngoing>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemOngoingBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, ongoingposition: Int) {
        val taskHolderAdd = holder as OngoingRecyclerViewAdapter.ViewHolder

        taskHolderAdd.binding.apply {
            viewModel = ongoingViewModel
            data = ongoinglist[ongoingposition]
            position = ongoingposition
        }
    }

    override fun getItemCount(): Int = ongoinglist.size

    inner class ViewHolder(val binding: ItemOngoingBinding):
            RecyclerView.ViewHolder(binding.root)
}