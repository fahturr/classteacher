package com.konexus.classteacher.ui.school

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivitySchoolBinding
import com.konexus.classteacher.ui.teacher.EventActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolActivity : AppCompatActivity() {

    private lateinit var schoolBinding: ActivitySchoolBinding
    private lateinit var schoolAdapter: SchoolRecyclerViewAdapter

    private val schoolViewModel: SchoolViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        schoolBinding = DataBindingUtil.setContentView(this, R.layout.activity_school)
        schoolAdapter = SchoolRecyclerViewAdapter(schoolViewModel)

        schoolBinding.apply {
            lifecycleOwner = this@SchoolActivity
            viewModel = schoolViewModel

            rvSchool.apply {
                adapter = schoolAdapter
                layoutManager = LinearLayoutManager(
                    this@SchoolActivity,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            }

            val helper = LinearSnapHelper()
            helper.attachToRecyclerView(rvSchool)
        }

        schoolViewModel.action.observe(this) { action ->
            when (action) {
                SchoolViewModel.ACTION_SCHOOL_LIST_UPDATED -> listUpdated()
                SchoolViewModel.ACTION_SCHOOL_ITEMSCHOOL_ONCLICK -> itemSchoolOnClick()
                SchoolViewModel.ACTION_SCHOOL_ITEMADDSCHOOL_ONCLICK -> itemAddSchoolOnClick()
            }
        }
    }

    private fun itemSchoolOnClick() {
        val namaSekolah =
            schoolViewModel.listSchool[(schoolViewModel.positionItemSchool.value?.minus(1)) ?: 0]

        startActivity(Intent(this, EventActivity::class.java))
    }

    private fun itemAddSchoolOnClick() {
        Toast.makeText(this, "Anda mengklik add button", Toast.LENGTH_SHORT).show()
    }

    private fun listUpdated() {
        schoolAdapter.listSchool.addAll(schoolViewModel.listSchool)
        schoolAdapter.notifyDataSetChanged()
    }


}