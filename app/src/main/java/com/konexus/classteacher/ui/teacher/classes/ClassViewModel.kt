package com.konexus.classteacher.ui.teacher.classes

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.R
import com.konexus.classteacher.model.Classes
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ClassViewModel : BaseViewModel() {

    companion object {
        const val ACTION_LIST_CLASS_UPDATED = "action_list_updated"
        const val ACTION_ITEM_ADD_ONCLICK = "action_item_add_onclick"
        const val ACTION_ITEM_CLASS_ONCLICK = "action_item_class_onclick"
        const val ACTION_SCHEDULE_ONCLICK = "action_schedule_onclick"
        const val ACTION_ASSIGNMENT_ONCLICK = "action_assignment_onclick"
        const val ACTION_QUIZEZ_ONCLICK = "action_quizez_onclick"
        const val ACTION_DISCUSSION_ONCLICK = "action_discussion_onclick"
        const val ACTION_MATERIAL_ONCLICK = "action_material_onclick"
    }

    val listClasses = arrayListOf<Classes>()

    val itemPosition = MutableLiveData<Int>()
    val classNameTitle = MutableLiveData<String>()

    fun fetchItem() {
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2000L)

            listClasses.clear()
            listClasses.addAll(getClasses())

            action.postValue(ACTION_LIST_CLASS_UPDATED)
            loadingEnabled.postValue(false)
        }
    }

    private fun getClasses(): ArrayList<Classes> {
        val tempClassesList = arrayListOf<Classes>()

        tempClassesList.add(Classes("Science", R.drawable.science))
        tempClassesList.add(Classes("English", R.drawable.english))
        tempClassesList.add(Classes("History", R.drawable.history))
        tempClassesList.add(Classes("Math", R.drawable.math))
        tempClassesList.add(Classes("Fine Art", R.drawable.fine_art))
        tempClassesList.add(Classes("Bahasa", R.drawable.bahasa))

        return tempClassesList
    }

    fun itemClassesOnClick(position: Int) {
        itemPosition.value = position - 1
        action.value = ACTION_ITEM_CLASS_ONCLICK
    }

    fun itemAddOnClick() {
        action.value = ACTION_ITEM_ADD_ONCLICK
    }

    fun assignmentOnClick() {
        action.value = ACTION_ASSIGNMENT_ONCLICK
    }

    fun quizezOnClick() {
        action.value = ACTION_QUIZEZ_ONCLICK
    }

    fun discussionBoardOnClick() {
        action.value = ACTION_DISCUSSION_ONCLICK
    }

    fun materialOnClick() {
        action.value = ACTION_MATERIAL_ONCLICK
    }

    fun scheduleOnclick(){
        action.value = ACTION_SCHEDULE_ONCLICK
    }
}