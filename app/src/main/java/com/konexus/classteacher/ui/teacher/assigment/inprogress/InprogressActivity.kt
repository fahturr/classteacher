package com.konexus.classteacher.ui.teacher.assigment.inprogress

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityInprogressBinding

class InprogressActivity : AppCompatActivity() {

    private lateinit var inprogressBinding: ActivityInprogressBinding
    private val inprogressViewModel: InprogressViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inprogressBinding =
        DataBindingUtil.setContentView(this,R.layout.activity_inprogress)

        inprogressBinding.apply {
            lifecycleOwner = this@InprogressActivity
            viewModel = inprogressViewModel
        }

        inprogressViewModel.action.observe(this, {action ->
            when(action){
                InprogressViewModel.BACK_ONCLICK -> backOnclick()
            }
        })
    }

    private fun backOnclick() {
        finish()
    }
}