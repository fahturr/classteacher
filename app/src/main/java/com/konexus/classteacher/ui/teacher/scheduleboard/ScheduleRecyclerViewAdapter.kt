package com.konexus.classteacher.ui.teacher.scheduleboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemJadwalBinding
import com.konexus.classteacher.model.Schedule

class ScheduleRecyclerViewAdapter (
    private val scheduleViewModel : ScheduleBoardViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    val listSchedule = arrayListOf<Schedule>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
        ItemJadwalBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, itemposition: Int) {
            val itemHolderAdd = holder as ScheduleRecyclerViewAdapter.ViewHolder

            itemHolderAdd.binding.apply {
                viewModel = scheduleViewModel
                data = listSchedule[itemposition]
                position = itemposition
            }
    }

    override fun getItemCount(): Int = listSchedule.size

    inner class ViewHolder(val binding: ItemJadwalBinding) :
        RecyclerView.ViewHolder(binding.root)
}