package com.konexus.classteacher.ui.teacher.assigment.complete

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemTaskcompleteBinding
import com.konexus.classteacher.model.TaskComplete

class CompleteRecyclerViewAdapter (
    private val completeViewModel: CompleteViewModel
        ): RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    val completelist = arrayListOf<TaskComplete>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemTaskcompleteBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, completeposition: Int) {
        val completeHolderAdd = holder as CompleteRecyclerViewAdapter.ViewHolder

        completeHolderAdd.binding.apply {
            viewModel = completeViewModel
            data = completelist[completeposition]
            position = completeposition
        }
    }

    override fun getItemCount(): Int = completelist.size

    inner class  ViewHolder(val binding: ItemTaskcompleteBinding):
            RecyclerView.ViewHolder(binding.root)

}