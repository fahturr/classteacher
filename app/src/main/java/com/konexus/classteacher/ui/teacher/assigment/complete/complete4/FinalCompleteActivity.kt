package com.konexus.classteacher.ui.teacher.assigment.complete.complete4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityFinalCompleteBinding
import com.konexus.classteacher.ui.teacher.assigment.HomeworkTaskListActivity
import com.konexus.classteacher.ui.teacher.assigment.complete.complete2.CorectCompleteActivity

class FinalCompleteActivity : AppCompatActivity() {

    private lateinit var finalCompleteBinding: ActivityFinalCompleteBinding
    private val finalCompleteViewModel: FinalCompleteViewModel by viewModels()
    private lateinit var adapterFinal: FinalCompleteRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        finalCompleteBinding =
        DataBindingUtil.setContentView(this,R.layout.activity_final_complete)

        finalCompleteBinding.apply {
            lifecycleOwner = this@FinalCompleteActivity
            viewModel = finalCompleteViewModel
        }

        adapterFinal = FinalCompleteRecyclerViewAdapter(finalCompleteViewModel)

        setupRecyclerView()

        finalCompleteViewModel.action.observe(this, {action ->
            when(action){
                FinalCompleteViewModel.BACK_ONCLICK -> backOncline()
                FinalCompleteViewModel.ACTION_UPDATE_LIST_COMPLETE4 -> actionUpdateListComplete4()
            }
        })
        finalCompleteViewModel.fetchComplete4List()
    }

    private fun actionUpdateListComplete4() {
        adapterFinal.apply {
            finalcompletelist.addAll(finalCompleteViewModel.complete4list)
            notifyDataSetChanged()
        }
    }

    private fun backOncline() {
        startActivity(Intent(this, HomeworkTaskListActivity::class.java))
        finish()
    }

    private fun setupRecyclerView() {
        finalCompleteBinding.recyclerfinalcomplete.apply {
            adapter = adapterFinal
            layoutManager = LinearLayoutManager(
                this@FinalCompleteActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }
}