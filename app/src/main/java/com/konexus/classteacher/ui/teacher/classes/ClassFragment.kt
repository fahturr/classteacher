package com.konexus.classteacher.ui.teacher.classes

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.FragmentClassBinding
import com.konexus.classteacher.ui.teacher.EventActivity
import com.konexus.classteacher.ui.teacher.assigment.HomeworkTaskListActivity
import com.konexus.classteacher.ui.teacher.discuss.DiscussActivity
import com.konexus.classteacher.ui.teacher.material.MaterialActivity
import com.konexus.classteacher.ui.teacher.scheduleboard.ScheduleBoardActivity

class ClassFragment : Fragment() {

    private val classViewModel: ClassViewModel by viewModels()
    private lateinit var binding: FragmentClassBinding

    private lateinit var adapterClasses: ClassRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentClassBinding.inflate(inflater, container, false)

        adapterClasses = ClassRecyclerViewAdapter(classViewModel)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val eventActivity = requireActivity() as EventActivity

        eventActivity.apply {
            supportActionBar?.apply {
                setBackgroundDrawable(
                    ColorDrawable(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                )
            }
            materialToolbar.setNavigationIcon(
                R.drawable.ic_menu_black
            )
        }

        binding.apply {
            lifecycleOwner = this@ClassFragment
            viewModel = classViewModel
        }

        setupRecyclerView()

        classViewModel.action.observe(viewLifecycleOwner, { action ->
            when (action) {
                ClassViewModel.ACTION_LIST_CLASS_UPDATED -> listClasessUpdated()
                ClassViewModel.ACTION_ITEM_CLASS_ONCLICK -> itemClassOnClick()
                ClassViewModel.ACTION_SCHEDULE_ONCLICK -> scheduleOnclick()
                ClassViewModel.ACTION_ASSIGNMENT_ONCLICK -> assignmentClassOnClick()
                ClassViewModel.ACTION_QUIZEZ_ONCLICK -> quizezOnClick()
                ClassViewModel.ACTION_DISCUSSION_ONCLICK -> discussionOnClick()
                ClassViewModel.ACTION_MATERIAL_ONCLICK -> materialOnClick()
            }
        })
        classViewModel.fetchItem()
    }

    private fun scheduleOnclick() {
        startActivity(Intent(requireContext(), ScheduleBoardActivity::class.java))
    }

    override fun onResume() {
        super.onResume()

        val eventActivity = requireActivity() as EventActivity

        eventActivity.apply {
            supportActionBar?.apply {
                setBackgroundDrawable(
                    ColorDrawable(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                )
            }
            materialToolbar.setNavigationIcon(
                R.drawable.ic_menu_black
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        val eventActivity = requireActivity() as EventActivity

        eventActivity.supportActionBar?.apply {
            setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.orange
                    )
                )
            )
        }
    }

    private fun listClasessUpdated() {
        adapterClasses.apply {
            listClasses.addAll(classViewModel.listClasses)
            notifyDataSetChanged()
        }
    }

    private fun setupRecyclerView() {
        binding.rvClasses.apply {
            adapter = adapterClasses
            layoutManager = GridLayoutManager(requireContext(), 3)
        }
    }

    private fun itemClassOnClick() {
        val data = classViewModel.listClasses[classViewModel.itemPosition.value ?: 0]
        classViewModel.classNameTitle.value = data.title

        val bottomClassSheet = BottomSheetClass(classViewModel)
        bottomClassSheet.show(childFragmentManager, BottomSheetClass.TAG)
    }

    private fun assignmentClassOnClick() {
        val data = classViewModel.listClasses[classViewModel.itemPosition.value ?: 0]

        startActivity(Intent(requireContext(), HomeworkTaskListActivity::class.java))
    }

    private fun quizezOnClick() {
        val data = classViewModel.listClasses[classViewModel.itemPosition.value ?: 0]

        // masukkan intent disini
    }

    private fun discussionOnClick() {
        val data = classViewModel.listClasses[classViewModel.itemPosition.value ?: 0]

        startActivity(Intent(requireContext(), DiscussActivity::class.java))
    }

    private fun materialOnClick() {
        val data = classViewModel.listClasses[classViewModel.itemPosition.value ?: 0]

        startActivity(Intent(requireContext(), MaterialActivity::class.java))
    }

}