package com.konexus.classteacher.ui.teacher.material.view

import com.konexus.classteacher.BaseViewModel

class ViewViewModel : BaseViewModel() {

    companion object{

        const val BACK_ONCLICK = "back_onclick"
    }

    fun backOnclick(){
        action.value = BACK_ONCLICK
    }
}