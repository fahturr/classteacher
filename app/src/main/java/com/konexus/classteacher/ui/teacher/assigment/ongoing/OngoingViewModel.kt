package com.konexus.classteacher.ui.teacher.assigment.ongoing

import androidx.core.app.NotificationCompat.getOngoing
import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.R
import com.konexus.classteacher.model.TaskOngoing
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class OngoingViewModel : BaseViewModel() {

    companion object{

        const val ACTION_BACK_ONCLICK = "action_back_onclick"
        const val ONGOING_TASK_LIST_UPDATE = "action_task_list_update"
    }

    val ongoingList = arrayListOf<TaskOngoing>()

    fun backOnclick(){
        action.value = ACTION_BACK_ONCLICK
    }

    fun fetchOngoingList(){
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2500L)

            val tempTaskOngoing = getOngoing()

            ongoingList.clear()
            ongoingList.addAll(tempTaskOngoing)

            action.postValue(ONGOING_TASK_LIST_UPDATE)
            loadingEnabled.postValue(false)
        }
    }

    private fun getOngoing(): ArrayList<TaskOngoing> {
        val getongoing = arrayListOf<TaskOngoing>()

        getongoing.add(
            TaskOngoing(
                R.drawable.john,
                "John D",
                "Statistika"
            )
        )

        getongoing.add(
            TaskOngoing(
                R.drawable.nina,
                "Nina",
                "Statistika"
            )
        )

        return getongoing
    }
}