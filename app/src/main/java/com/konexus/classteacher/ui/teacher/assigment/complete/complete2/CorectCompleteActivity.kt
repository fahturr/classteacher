package com.konexus.classteacher.ui.teacher.assigment.complete.complete2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityCorectCompleteBinding
import com.konexus.classteacher.ui.teacher.assigment.complete.complete3.ScoreCompleteActivity

class CorectCompleteActivity : AppCompatActivity() {

    private lateinit var corectCompleteActivityBinding: ActivityCorectCompleteBinding
    private val corectCompleteViewModel: CorectCompleteViewModel by viewModels()
    private lateinit var adaptercorect: CorectCompleteRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        corectCompleteActivityBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_corect_complete)

        corectCompleteActivityBinding.apply {
            lifecycleOwner = this@CorectCompleteActivity
            viewModel = corectCompleteViewModel
        }
        adaptercorect = CorectCompleteRecyclerViewAdapter(corectCompleteViewModel)

        setupRecyclerView()

        corectCompleteViewModel.action.observe(this, { action ->
            when (action) {
                CorectCompleteViewModel.BACK_ONCLICK -> backOnclick()
                CorectCompleteViewModel.ACTION_UPDATE_LIST_COMPLETE2 -> actionUpdateListComplete2()
                CorectCompleteViewModel.ACTION_SCORECOMPLETE_ONCLICK -> actionScoreCompleteOnclick()
            }
        })
        corectCompleteViewModel.fetchComplete2List()
    }

    private fun actionScoreCompleteOnclick() {
        startActivity(Intent(this, ScoreCompleteActivity::class.java))
    }

    private fun actionUpdateListComplete2() {
        adaptercorect.apply {
            corectcompletelist.addAll(corectCompleteViewModel.complete2list)
            notifyDataSetChanged()
        }
    }

    private fun backOnclick() {
        finish()
    }

    private fun setupRecyclerView() {
        corectCompleteActivityBinding.recyclercorection.apply {
            adapter = adaptercorect
            layoutManager = LinearLayoutManager(
                this@CorectCompleteActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }
}