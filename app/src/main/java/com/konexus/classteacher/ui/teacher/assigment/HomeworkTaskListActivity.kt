package com.konexus.classteacher.ui.teacher.assigment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityHomeworkTaskListBinding
import com.konexus.classteacher.model.TaskList
import com.konexus.classteacher.ui.teacher.EventActivity
import com.konexus.classteacher.ui.teacher.assigment.complete.CompleteActivity
import com.konexus.classteacher.ui.teacher.assigment.inprogress.InprogressActivity
import com.konexus.classteacher.ui.teacher.assigment.ongoing.OnGoingActivity
import com.konexus.classteacher.ui.teacher.classes.ClassFragment
import com.konexus.classteacher.ui.teacher.scheduleboard.ScheduleBoardActivity

class HomeworkTaskListActivity : AppCompatActivity() {

    private lateinit var homeworkTaskListBinding: ActivityHomeworkTaskListBinding
    private val homeworkTaskListViewModel: HomeworkTaskListViewModel by viewModels()
    private lateinit var adapterTask: TaskListRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeworkTaskListBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_homework_task_list)

        homeworkTaskListBinding.apply {
            lifecycleOwner = this@HomeworkTaskListActivity
            viewModel = homeworkTaskListViewModel
        }

        adapterTask = TaskListRecyclerViewAdapter(homeworkTaskListViewModel)

        setupRecycleeViewTask()

        homeworkTaskListViewModel.action.observe(this) { action ->
            when (action) {
                HomeworkTaskListViewModel.BACK_ONCLICK_HOMEWORKTASKLIST -> backOnclickHomeworkTaskList()
                HomeworkTaskListViewModel.COMPLETE_TASK_ONCLICK -> completeTaskOnclick()
                HomeworkTaskListViewModel.ACTION_TASK_LIST_UPDATE -> actionTaskListUpdate()
                HomeworkTaskListViewModel.INPROGRESS_TASK_ONCLICK -> inprogressTaskOnclick()
                HomeworkTaskListViewModel.ONGOING_TASK_ONCLICK -> ongoingTaskOnclick()
            }
        }
        homeworkTaskListViewModel.fetchTaskList()
    }

    private fun ongoingTaskOnclick() {
        startActivity(Intent(this, OnGoingActivity::class.java))
    }

    private fun inprogressTaskOnclick() {
        startActivity(Intent(this, InprogressActivity::class.java))
    }

    private fun actionTaskListUpdate(){
        adapterTask.apply {
            tasklist.addAll(homeworkTaskListViewModel.taskList)
            notifyDataSetChanged()
        }
    }

    private fun  backOnclickHomeworkTaskList(){
        startActivity(Intent(this, EventActivity::class.java))
    }

    private fun completeTaskOnclick(){
        startActivity(Intent(this, CompleteActivity::class.java))
    }

    private fun setupRecycleeViewTask(){
        homeworkTaskListBinding.recyclertask.apply {
            adapter = adapterTask
            layoutManager = LinearLayoutManager(
                this@HomeworkTaskListActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }
}