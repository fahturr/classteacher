package com.konexus.classteacher.ui.teacher.discuss

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.R
import com.konexus.classteacher.model.Discuss
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DiscussViewModel : BaseViewModel() {

    companion object{
        const val BACK_ONCLICK = "back_onclick"
        const val CAMERA_ONCLICK = "camera_onclick"
        const val GALLERY_ONCLICK = "gallery_onclick"
        const val LOCATION_ONCLICK = "location_onclick"
        const val DOCUMENT_ONCLICK = "document_onclick"
        const val TAG_ONCLICK = "tag_onclick"
        const val COMENT_ONCLICK = "coment_onclick"
        const val ACTION_DISCUSS_LIST_UPDATE = "action_discuss_list_update"
        const val ADD_POST_LIST = "add_post_list"
    }

    val discusslist = arrayListOf<Discuss>()

    val discussposition = MutableLiveData<Int>()

    fun addPostList(){
        action.value = ADD_POST_LIST
    }

    fun backOnclick(){
        action.value = BACK_ONCLICK
    }

    fun cameraOnclick(){
        action.value = CAMERA_ONCLICK
    }

    fun galleryOnclick(){
        action.value = GALLERY_ONCLICK
    }

    fun locationOnclick(){
        action.value = LOCATION_ONCLICK
    }

    fun documentOnclick(){
        action.value = DOCUMENT_ONCLICK
    }

    fun tagOnclick(){
        action.value = TAG_ONCLICK
    }

    fun comentOnclick(){
        action.value = COMENT_ONCLICK
    }

    fun fetchDiscussList(){
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2500L)

            val tempDiscussList = getDiscuss()

            discusslist.clear()
            discusslist.addAll(tempDiscussList)

            action.postValue(ACTION_DISCUSS_LIST_UPDATE)
            loadingEnabled.postValue(false)
        }
    }

    private fun getDiscuss(): ArrayList<Discuss> {
        val getdiscuss = arrayListOf<Discuss>()

        getdiscuss.add(
            Discuss(
                R.drawable.john,
                "Kevin Ryan",
                "9 Hours",
                "Lorem Ipsum",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non",
                null
            )
        )

        getdiscuss.add(
            Discuss(
                R.drawable.nina,
                "Nina",
                "11 Hours",
                "Lorem Ipsum",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non",

            )
        )

        return getdiscuss
    }
}