package com.konexus.classteacher.ui.teacher.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.R
import com.konexus.classteacher.model.Event
import com.konexus.classteacher.model.Image
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class HomeViewModel : BaseViewModel() {

    companion object {
        const val ACTION_LIST_IMAGE_UPDATED = "action_list_image_updated"
        const val ACTION_LIST_EVENT_UPDATED = "action_list_event_updated"
        const val ACTION_LIST_IMAGE_CLICK = "action_list_image_click"
        const val ACTION_LIST_EVENT_CLICK = "action_list_event_click"
        const val ACTION_LIST_EVENTADD_CLICK = "action_list_eventadd_click"
    }

    val listImage = arrayListOf<Image>()
    val listEvent = arrayListOf<Event>()

    val imagePosition = MutableLiveData<Int>()
    val eventPosition = MutableLiveData<Int>()

    fun itemImageOnClick(position: Int) {
        imagePosition.value = position
        action.value = ACTION_LIST_IMAGE_CLICK
    }

    fun itemEventOnClick(position: Int) {
        eventPosition.value = position
        action.value = ACTION_LIST_EVENT_CLICK
    }

    fun itemEventAddOnClick() {
        action.value = ACTION_LIST_EVENTADD_CLICK
    }

    fun fetchImageList() {
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2000L)

            val tempListImage = getImage()

            listImage.clear()
            listImage.addAll(tempListImage)

            action.postValue(ACTION_LIST_IMAGE_UPDATED)
            loadingEnabled.postValue(false)
        }
    }

    fun fetchEventList() {
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2500L)

            val tempListEvent = getEvent()

            listEvent.clear()
            listEvent.addAll(tempListEvent)

            action.postValue(ACTION_LIST_EVENT_UPDATED)
            loadingEnabled.postValue(false)
        }
    }

    fun getImage(): ArrayList<Image> {
        val tempImageList = arrayListOf<Image>()

        tempImageList.add(Image(R.drawable.sman1_sleman))
        tempImageList.add(Image(R.drawable.sman54_jakarta))

        return tempImageList
    }

    fun getEvent(): ArrayList<Event> {
        val tempImageList = arrayListOf<Event>()

        tempImageList.add(Event(R.drawable.covid_procedure))
        tempImageList.add(Event(R.drawable.orang_bijak))

        return tempImageList
    }

}