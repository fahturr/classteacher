package com.konexus.classteacher.ui.teacher.profile.menu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivityAboutBinding

class AboutActivity : AppCompatActivity() {

    private lateinit var aboutActivityBinding: ActivityAboutBinding
    private val aboutViewModel : AboutViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        aboutActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_about)

        aboutActivityBinding.apply {
            lifecycleOwner = this@AboutActivity
            viewModel = aboutViewModel
        }

        aboutViewModel.action.observe(this, { action ->
            when(action){
                AboutViewModel.BACK_ONCLICK_PROFILE -> backOnclickProfile()
            }
        })
    }

    private fun backOnclickProfile() {
        finish()

    }
}
