package com.konexus.classteacher.ui.teacher.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.konexus.classteacher.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private val homeViewModel: HomeViewModel by viewModels()
    private var _binding: FragmentHomeBinding? = null

    private lateinit var adapterImage: ImageRecyclerViewAdapter
    private lateinit var adapterEvent: EventRecyclerViewAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        adapterImage = ImageRecyclerViewAdapter(homeViewModel)
        adapterEvent = EventRecyclerViewAdapter(homeViewModel)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            lifecycleOwner = this@HomeFragment
            viewModel = homeViewModel
        }

        setupRecyclerView()

        homeViewModel.action.observe(viewLifecycleOwner, { action ->
            when (action) {
                HomeViewModel.ACTION_LIST_IMAGE_UPDATED -> listImageUpdated()
                HomeViewModel.ACTION_LIST_EVENT_UPDATED -> listEventUpdated()
                HomeViewModel.ACTION_LIST_EVENT_CLICK -> itemEventOnClick()
                HomeViewModel.ACTION_LIST_IMAGE_CLICK -> itemImageOnClick()
                HomeViewModel.ACTION_LIST_EVENTADD_CLICK -> itemEventAddOnClick()
            }
        })

        homeViewModel.apply {
            fetchImageList()
            fetchEventList()
        }
    }

    private fun itemImageOnClick() {
        Toast.makeText(requireContext(), "Image School clicked!", Toast.LENGTH_SHORT).show()
    }

    private fun itemEventAddOnClick() {
        Toast.makeText(requireContext(), "Add Event!", Toast.LENGTH_SHORT).show()
    }

    private fun itemEventOnClick() {
        Toast.makeText(requireContext(), "Event Clicked", Toast.LENGTH_SHORT).show()
    }

    private fun listEventUpdated() {
        adapterEvent.apply {
            listEvent.addAll(homeViewModel.listEvent)
            notifyDataSetChanged()
        }
    }

    private fun listImageUpdated() {
        adapterImage.apply {
            listImage.addAll(homeViewModel.listImage)
            notifyDataSetChanged()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupRecyclerView() {
        binding.apply {
            rvEvent.apply {
                adapter = adapterEvent
                layoutManager = LinearLayoutManager(
                    requireContext(),
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            }
            rvImage.apply {
                adapter = adapterImage
                layoutManager = LinearLayoutManager(
                    requireContext(),
                    LinearLayoutManager.HORIZONTAL,
                    false
                )

                val snapHelper = PagerSnapHelper()

                snapHelper.attachToRecyclerView(rvImage)
                cirindRvimage.attachToRecyclerView(rvImage, snapHelper)

                adapterImage.registerAdapterDataObserver(cirindRvimage.adapterDataObserver)
            }
        }
    }

}