package com.konexus.classteacher.ui.teacher.assigment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemHomeworkTaskListBinding
import com.konexus.classteacher.model.TaskList

class TaskListRecyclerViewAdapter (
    private val homeworkTaskListViewModel: HomeworkTaskListViewModel
        ) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

            val tasklist = arrayListOf<TaskList>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemHomeworkTaskListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, taskposition: Int) {
        val taskHolderAdd = holder as TaskListRecyclerViewAdapter.ViewHolder

        taskHolderAdd.binding.apply {
            viewModel = homeworkTaskListViewModel
            data = tasklist[taskposition]
            position = taskposition
        }
    }

    override fun getItemCount(): Int = tasklist.size

    inner class ViewHolder(val binding: ItemHomeworkTaskListBinding):
            RecyclerView.ViewHolder(binding.root)

}