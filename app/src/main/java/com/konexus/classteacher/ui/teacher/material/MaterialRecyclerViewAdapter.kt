package com.konexus.classteacher.ui.teacher.material

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.konexus.classteacher.databinding.ItemMaterialBinding
import com.konexus.classteacher.model.Material

class MaterialRecyclerViewAdapter (
    private val materialViewModel: MaterialViewModel
        ): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val materiallist = arrayListOf<Material>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            ItemMaterialBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, materialposition: Int) {
       val materialHolderAdd = holder as MaterialRecyclerViewAdapter.ViewHolder
        val itemMaterial = materiallist[materialposition]

        materialHolderAdd.binding.apply {
            viewModel = materialViewModel
            data = itemMaterial
            position = materialposition
            recyclersubMaterial.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = SubMaterialRecyclerViewAdapter(materialViewModel, itemMaterial.listSubMaterial)
                setRecycledViewPool(RecyclerView.RecycledViewPool())
            }
        }
    }

    override fun getItemCount(): Int  = materiallist.size

    inner class ViewHolder(val binding: ItemMaterialBinding):
            RecyclerView.ViewHolder(binding.root)

}