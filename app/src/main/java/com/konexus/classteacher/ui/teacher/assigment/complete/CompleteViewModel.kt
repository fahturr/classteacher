package com.konexus.classteacher.ui.teacher.assigment.complete

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.model.TaskComplete
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CompleteViewModel : BaseViewModel() {
    companion object{

        const val BACK_ONCLICK = "back_onclick"
        const val ACTION_COMPLETE2_ONCLICK = "action_complete2_onclick"
        const val ACTION_UPDATE_LIST_COMPLETE1 = "action_update_list_complete1"
    }

    val complete1list = arrayListOf<TaskComplete>()

    val completeposition = MutableLiveData<Int>()

    fun backOnclick(){
        action.value = BACK_ONCLICK
    }
    fun complete2Onclick(){

        action.value = ACTION_COMPLETE2_ONCLICK
    }

    fun fetchComplete1List(){
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2500L)

            val tempListComplete1 = getCompleete1()

            complete1list.clear()
            complete1list.addAll(tempListComplete1)

            action.postValue(ACTION_UPDATE_LIST_COMPLETE1)
            loadingEnabled.postValue(false)
        }
    }

    private fun getCompleete1(): ArrayList<TaskComplete> {
        val getcomplete1 = arrayListOf<TaskComplete>()

        getcomplete1.add(
            TaskComplete(
                "Statistika",
                "Competence 1"
            )
        )
        getcomplete1.add(
            TaskComplete(
                "Statistika",
                "Competence 2"
            )
        )
        getcomplete1.add(
            TaskComplete(
                "Statistika",
                "Competence 3"
            )
        )
        getcomplete1.add(
            TaskComplete(
                "Statistika",
                "Competence 4"
            )
        )

        return getcomplete1
    }

}