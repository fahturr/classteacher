package com.konexus.classteacher.ui.teacher.material

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.R
import com.konexus.classteacher.model.Material
import com.konexus.classteacher.model.SubMaterial
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MaterialViewModel : BaseViewModel() {

    companion object {

        const val BACK_ONCLICK = "back_onclick"
        const val ACTION_UPDATE_LIST_MATERIAL = "action_update_list_material"
        const val ACTION_SUBMATERIAL_ONCLICK = "action_submaterial_onclick"
    }

    val materiallist = arrayListOf<Material>()
    val submateriallist = arrayListOf<SubMaterial>()

    val materialposition = MutableLiveData<Int>()
    val submaterialposition = MutableLiveData<Int>()

    fun backOnclick(){
        action.value = BACK_ONCLICK
    }

    fun submaterialOnclick(position:Int){
        materialposition.value = position
        action.value = ACTION_SUBMATERIAL_ONCLICK

    }

    fun  fetchMaterialList(){
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2500L)

            val tempListMaterial = getMaterial()

            materiallist.clear()
            materiallist.addAll(tempListMaterial)

            action.postValue(ACTION_UPDATE_LIST_MATERIAL)
            loadingEnabled.postValue(false)
        }
    }

    private fun getMaterial(): ArrayList<Material> {
        val getmaterial = arrayListOf<Material>()

        getmaterial.add(
            Material(
                "Statistika",
                "22/Aug/2021",
                R.drawable.imageplusmaterial,
                listOf(
                    SubMaterial(
                        "Statistika", R.drawable.imagevideosubmaterial,
                    "Statistika", R.drawable.imagedownloadsubmaterial),
                )
            )
        )
        getmaterial.add(
            Material(
                "Bilangan",
                "22/Aug/2021",
                R.drawable.imageplusmaterial,
                listOf(
                    SubMaterial(
                        "Bilangan", R.drawable.imagevideosubmaterial,
                        "Bilangan",R.drawable.imagedownloadsubmaterial),
                )
            )
        )
        getmaterial.add(
            Material(
                "Aljabar",
                "22/Aug/2021",
                R.drawable.imageplusmaterial,
                listOf(
                    SubMaterial(
                        "Aljabar", R.drawable.imagevideosubmaterial,
                        "Aljabar",R.drawable.imagedownloadsubmaterial),
                )
            )
        )

        return getmaterial
    }
}