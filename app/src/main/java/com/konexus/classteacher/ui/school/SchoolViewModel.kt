package com.konexus.classteacher.ui.school

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.R
import com.konexus.classteacher.model.School
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(
) : BaseViewModel() {

    companion object {
        const val ACTION_SCHOOL_LIST_UPDATED = "action_school_list_updated"
        const val ACTION_SCHOOL_ITEMSCHOOL_ONCLICK = "action_school_itemschool_onclick"
        const val ACTION_SCHOOL_ITEMADDSCHOOL_ONCLICK = "action_school_itemaddschool_onclick"
    }

    val listSchool = arrayListOf<School>()
    val positionItemSchool = MutableLiveData<Int>()

    init {
        loadingEnabled.value = true
        fetchSchool()
    }

    fun fetchSchool() {
        viewModelScope.launch {
            delay(2000L)

            listSchool.clear()
            listSchool.addAll(getSchool())

            loadingEnabled.postValue(false)
            action.postValue(ACTION_SCHOOL_LIST_UPDATED)
        }
    }

    fun getSchool(): ArrayList<School> {
        val tempSchoolList = arrayListOf<School>()

        tempSchoolList.add(School(R.drawable.sman1_sleman, "SMAN1 Sleman"))
        tempSchoolList.add(School(R.drawable.sman54_jakarta, "SMAN54 Jakarta"))

        return tempSchoolList
    }

    fun onItemSchoolClick(position: Int) {
        positionItemSchool.value = position
        action.value = ACTION_SCHOOL_ITEMSCHOOL_ONCLICK
    }

    fun onItemAddShoolClick() {
        action.value = ACTION_SCHOOL_ITEMADDSCHOOL_ONCLICK
    }

}