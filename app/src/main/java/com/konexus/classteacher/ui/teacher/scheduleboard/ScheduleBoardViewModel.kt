package com.konexus.classteacher.ui.teacher.scheduleboard

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.konexus.classteacher.BaseViewModel
import com.konexus.classteacher.model.Absent
import com.konexus.classteacher.model.Schedule
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import com.konexus.classteacher.R

class ScheduleBoardViewModel : BaseViewModel() {

    companion object {

      const val  BACK_ONCLICK_SCHEDULEBOARD = "back_onclick_scheduleboard"
        const val GET_IMAGE_SCHEDULEBOARD = "get_image_scheduleboard"
        const val ACTION_LIST_SCHEDULE_UPDATE = "action_list_schedule_update"
        const val ACTION_LIST_ABSENT_UPDATE = "action_list_absent_update"
        const val MAPEL1_ONCLICK = "mapel1_onclick"
        const val MAPEL2_ONCLICK = "mapel2_onclick"
        const val MAPEL3_ONCLICK = "mapel3_onclick"
        const val MAPEL4_ONCLICK = "mapel4_onclick"
        const val MAPEL5_ONCLICK = "mapel5_onclick"
        const val MAPEL6_ONCLICK = "mapel6_onclick"
        const val MAPEL7_ONCLICK = "mapel7_onclick"
        const val MAPEL8_ONCLICK = "mapel8_onclick"
        const val ABSEN_ONCLICK = "absen_onclick"
    }

    val listSchedule = arrayListOf<Schedule>()
    val listAbsent = arrayListOf<Absent>()

    val schedulePosition = MutableLiveData<Int>()
    val absenPosition = MutableLiveData<Int>()

    fun backOnclickScheduleBoard(){

        action.value = BACK_ONCLICK_SCHEDULEBOARD
    }

    fun absenOnclick(position: Int){
        absenPosition.value = position
        action.value = ABSEN_ONCLICK
    }

    fun fetchAbsentList() {
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(3000L)

            val tempListAbsent = getAbsent()

            listAbsent.clear()
            listAbsent.addAll(tempListAbsent)

            action.postValue(ACTION_LIST_ABSENT_UPDATE)
            loadingEnabled.postValue(false)
        }
    }

    fun getAbsent() : ArrayList<Absent> {
        val getabsen = arrayListOf<Absent>()

        getabsen.add(
            Absent(
            "1.",
        "Budi Doraemon",
            R.drawable.absentschedule
        )
        )
        getabsen.add(
            Absent(
                "2.",
                "Ahmad F",
                R.drawable.absentschedule
            )
        )
        getabsen.add(
            Absent(
                "3.",
                "Enda",
                R.drawable.absentschedule
            )
        )
        getabsen.add(
            Absent(
                "4.",
                "Kevin Ryan",
                R.drawable.absentschedule
            )
        )

    return getabsen
    }


    fun getImageScheduleBoard(){

        action.value = GET_IMAGE_SCHEDULEBOARD
    }

    fun mapel1Onclick(position : Int){
        schedulePosition.value = position
        action.value = MAPEL1_ONCLICK
    }
    fun mapel2Onclick(position : Int){
        schedulePosition.value = position
        action.value = MAPEL2_ONCLICK

    }
    fun mapel3Onclick(position : Int){
        schedulePosition.value = position
        action.value = MAPEL3_ONCLICK

    }
    fun mapel4Onclick(position : Int){
        schedulePosition.value = position
        action.value = MAPEL4_ONCLICK

    }
    fun mapel5Onclick(position : Int){
        schedulePosition.value = position
        action.value = MAPEL5_ONCLICK

    }
    fun mapel6Onclick(position : Int){
        schedulePosition.value = position
        action.value = MAPEL6_ONCLICK

    }
    fun mapel7Onclick(position : Int){
        schedulePosition.value = position
        action.value = MAPEL7_ONCLICK

    }
    fun mapel8Onclick(position : Int){
        schedulePosition.value = position
        action.value = MAPEL8_ONCLICK

    }
    fun fetchScheduleList() {
        viewModelScope.launch {
            loadingEnabled.postValue(true)
            delay(2500L)

            val tempListSchedule = getSchedule()

            listSchedule.clear()
            listSchedule.addAll(tempListSchedule)

            action.postValue(ACTION_LIST_SCHEDULE_UPDATE)
            loadingEnabled.postValue(false)
        }
    }

    fun getSchedule() : ArrayList<Schedule>{
        val getmapel = arrayListOf<Schedule>()

        getmapel.add(Schedule(
            judul= "Time",
            mapel1 = "07.00-08.00",
            mapel2 = "07.00-08.00",
            mapel3 = "07.00-08.00",
            mapel4 = "07.00-08.00",
            mapel5 = "07.00-08.00",
            mapel6 = "07.00-08.00",
            mapel7 = "07.00-08.00",
            mapel8 = "07.00-08.00"))
        getmapel.add(Schedule(
            judul= "Senin",
            mapel1 = "Sport1",
            mapel2 = "Sport1",
            mapel3 = "Sport1",
            mapel4 = "Sport1",
            mapel5 = "Sport1",
            mapel6 = "Sport1",
            mapel7 = "Sport1",
            mapel8 = "Sport1"))
        getmapel.add(Schedule(
            judul= "Selasa",
            mapel1 = "Sport1",
            mapel2 = "Sport1",
            mapel3 = "Sport1",
            mapel4 = "Sport1",
            mapel5 = "Sport1",
            mapel6 = "Sport1",
            mapel7 = "Sport1",
            mapel8 = "Sport1"))
        getmapel.add(Schedule(
            judul= "Rabu",
            mapel1 = "Sport1",
            mapel2 = "Sport1",
            mapel3 = "Sport1",
            mapel4 = "Sport1",
            mapel5 = "Sport1",
            mapel6 = "Sport1",
            mapel7 = "Sport1",
            mapel8 = "Sport1"))
        getmapel.add(Schedule(
            judul= "Kamis",
            mapel1 = "Sport1",
            mapel2 = "Sport1",
            mapel3 = "Sport1",
            mapel4 = "Sport1",
            mapel5 = "Sport1",
            mapel6 = "Sport1",
            mapel7 = "Sport1",
            mapel8 = "Sport1"))
        getmapel.add(Schedule(
            judul= "Jumat",
            mapel1 = "Sport1",
            mapel2 = "Sport1",
            mapel3 = "Sport1",
            mapel4 = "Sport1",
            mapel5 = "Sport1",
            mapel6 = "Sport1",
            mapel7 = "Sport1",
            mapel8 = "Sport1"))
        getmapel.add(Schedule(
            judul= "Sabtu",
            mapel1 = "Sport1",
            mapel2 = "Sport1",
            mapel3 = "Sport1",
            mapel4 = "Sport1",
            mapel5 = "Sport1",
            mapel6 = "Sport1",
            mapel7 = "Sport1",
            mapel8 = "Sport1"))

        return getmapel
    }
}