package com.konexus.classteacher.ui.teacher.profile.menu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.konexus.classteacher.R
import com.konexus.classteacher.databinding.ActivitySecurityBinding

class SecurityActivity : AppCompatActivity() {

    private lateinit var securitytActivityBinding: ActivitySecurityBinding
    private val securityViewModel : SecurityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        securitytActivityBinding= DataBindingUtil.setContentView(this, R.layout.activity_security)

        securitytActivityBinding.apply {
            lifecycleOwner = this@SecurityActivity
            viewModel = securityViewModel
        }

        securityViewModel.action.observe(this, { action ->
            when(action){
                AccountViewModel.BACK_ONCLICK_PROFILE -> backOnclickProfile()
            }
        })
    }

    private fun backOnclickProfile() {
        finish()

    }
}