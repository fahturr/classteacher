package com.konexus.classteacher.ui.teacher.assigment.complete.complete3

import com.konexus.classteacher.BaseViewModel

class ScoreCompleteViewModel : BaseViewModel(){

    companion object{

        const val BACK_ONCLICK = "back_onclick"
        const val ACTION_FINALCOMPLETE_ONCLICK = "action_finalcomplete_onclcik"
    }

    fun backOnclick(){
        action.value = BACK_ONCLICK
    }

    fun finalCompleteOnclick(){
        action.value = ACTION_FINALCOMPLETE_ONCLICK
    }
}